export default interface Expense {
  name: string;
  price: number;
  date: string;
  categoryId: number;
  id?: number;

  // eslint-disable-next-line semi
}
