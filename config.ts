export const colors = {
  darkest: '#0F0221',
  dark: '#251531',
  light: '#554C60',
  lightest: '#7E7984',
  white: '#EDEDED',
};
