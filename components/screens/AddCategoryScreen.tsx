import React, {useState, useContext} from 'react';
import {Text, View, TextInput, Button, StyleSheet} from 'react-native';
import DataContext from '../../contexts/DataContext';
import {colors} from '../../config';

const AddCategoryScreen = ({navigation}) => {
  const [text, setText] = useState('');
  const {dispatch} = useContext(DataContext);

  const onTextChange = nextText => {
    setText(nextText);
  };

  const onCancel = () => {
    navigation.navigate('Dashboard');
  };

  const onSubmit = () => {
    dispatch({
      type: 'ADD_CATEGORY',
      category: {
        name: text,
      },
    });

    setText('');
    onCancel();
  };

  return (
    <View style={styles.outerContainer}>
      <View style={styles.container}>
        <Text style={styles.text}>Create new Category</Text>
        <TextInput
          style={styles.input}
          autoFocus
          onChangeText={onTextChange}
          value={text}
          placeholder="Name"
        />
        <Button title="Create" onPress={onSubmit} />
      </View>
    </View>
  );
};

AddCategoryScreen.navigationOptions = {
  title: 'Add Category',
};

const styles = StyleSheet.create({
  outerContainer: {
    height: '100%',
    backgroundColor: colors.darkest,
  },
  container: {
    height: '40%',
    width: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: colors.darkest,
  },
  text: {
    color: colors.white,
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    width: '60%',
    height: 40,
    borderColor: colors.lightest,
    borderWidth: 1,
    backgroundColor: colors.white,
  },
});

export default AddCategoryScreen;
