import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors} from '../../../config';
import Expense from '../../../domain/expense.interface';
import Category from 'domain/category.interface';

type Props = {
  category: Category;
  expenses: Expense[];
};

const CategoryCard = ({category, expenses, navigation}: Props) => {
  const totalPrice = expenses.reduce((acc, curr) => acc + curr.price, 0);
  const {name} = category;

  const onPress = () => {
    navigation.navigate('ExpenseList', {
      category,
    });
  };

  return (
    <TouchableOpacity style={styles.cardContainer} onPress={onPress}>
      <View style={styles.innerContainer}>
        {/* TODO ICON */}
        <Text style={styles.text} />
        <Text style={[styles.text, styles.title]}>{name}</Text>
        <Text style={styles.text}>Total: {totalPrice}</Text>
        <View>
          {expenses.map(e => (
            <View key={e.id}>
              <Text style={styles.text}>
                {e.name}: {e.price}
              </Text>
            </View>
          ))}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: colors.light,
    flexDirection: 'column',
    height: 200,
    margin: 16,
    width: '40%',
    borderRadius: 16,
    padding: 16,
  },
  innerContainer: {
    height: '100%',
    overflow: 'hidden',
    flexGrow: 1,
  },
  text: {
    width: '100%',
    color: colors.white,
    marginVertical: 8,
    marginHorizontal: 'auto',
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default CategoryCard;
