import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {styles as categoryCardStyles} from './CategoryCard';

const AddCategoryCard = ({navigation}) => {
  const navigateToCreateCategory = () => {
    navigation.navigate('AddCategory');
  };

  return (
    <>
      <TouchableOpacity
        style={categoryCardStyles.cardContainer}
        onPress={navigateToCreateCategory}>
        <View style={categoryCardStyles.innerContainer}>
          <Text style={categoryCardStyles.text}>+</Text>
          <Text style={[categoryCardStyles.text, categoryCardStyles.title]}>
            Create Category
          </Text>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default AddCategoryCard;
