import React, {useContext} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import CategoryCard from './CategoryCard';
import AddCategoryCard from './AddCategoryCard';
import DataContext from '../../../contexts/DataContext';
import Category from '../../../domain/category.interface';
import {colors} from '../../../config';
import {getExpensesForCategory} from '../../../utilities/data';
import PieChartWithCenteredLabels from '../../plots/PieChartWithCenteredLabel';

const DashboardScreen = ({navigation}) => {
  const {state} = useContext(DataContext);
  const {categories, expenses} = state;

  const renderCategoryCard = (category: Category) => {
    const relevantExpenses = getExpensesForCategory(category.id, expenses);

    getExpensesForCategory;

    return (
      <CategoryCard
        key={category.id}
        category={category}
        expenses={relevantExpenses}
        navigation={navigation}
      />
    );
  };

  return (
    <>
      <View style={styles.topContainer}>
        <PieChartWithCenteredLabels />
        <ScrollView
          style={styles.scrollContainer}
          contentContainerStyle={styles.content}
          showsVerticalScrollIndicator
          keyboardShouldPersistTaps="always">
          <AddCategoryCard navigation={navigation} />
          {categories.map(category => renderCategoryCard(category))}
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  topContainer: {
    height: '100%',
    backgroundColor: colors.darkest,
  },
  scrollContainer: {
    padding: 16,
    paddingVertical: 40,
  },
  content: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: colors.white,
    fontSize: 32,
    margin: 16,
    fontWeight: 'bold',
    fontFamily: 'sans-serif',
  },
});

export default DashboardScreen;
