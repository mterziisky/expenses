import React, {useState, useContext} from 'react';
import {Text, View, TextInput, Button, StyleSheet} from 'react-native';
import DataContext from '../../contexts/DataContext';
import {colors} from '../../config';
import moment from 'moment';

const AddExpenseScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const {dispatch} = useContext(DataContext);

  const onNameChange = (nextName: string) => {
    setName(nextName);
  };

  const onPriceChange = (nextPrice: string) => {
    if (!Number.isNaN(Number(nextPrice))) {
      setPrice(nextPrice);
    }
  };

  const onCancel = () => {
    navigation.goBack();
  };

  const onSubmit = () => {
    const categoryId = navigation.getParam('category').id;

    dispatch({
      type: 'ADD_EXPENSE',
      expense: {
        name,
        price: Number(price),
        date: moment().format('YYYY-MM-DD'),
        categoryId,
      },
    });

    onCancel();
  };

  return (
    <View style={styles.outerContainer}>
      <View style={styles.container}>
        <Text style={styles.text}>Create new Category</Text>
        <TextInput
          style={styles.input}
          autoFocus
          onChangeText={onNameChange}
          value={name}
          placeholder="Name"
        />
        <TextInput
          style={styles.input}
          onChangeText={onPriceChange}
          value={price}
          placeholder="Price"
          keyboardType="numeric"
        />
        <Button title="Create" onPress={onSubmit} />
      </View>
    </View>
  );
};

AddExpenseScreen.navigationOptions = ({navigation}) => {
  const category = navigation.getParam('category');

  console.log('AES' + navigation.getParam('category'));

  return {
    title: `Add Expense to ${category.name}`,
  };
};

const styles = StyleSheet.create({
  outerContainer: {
    backgroundColor: colors.darkest,
    height: '100%',
  },
  container: {
    height: 300,
    width: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: colors.darkest,
  },
  text: {
    color: colors.white,
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    width: '60%',
    height: 40,
    borderColor: colors.lightest,
    borderWidth: 1,
    backgroundColor: colors.white,
  },
});

export default AddExpenseScreen;
