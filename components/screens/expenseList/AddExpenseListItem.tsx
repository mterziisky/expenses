import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {styles as expenseListStyles} from './ExpenseListItem';

const AddExpenseListItem = ({navigation}) => {
  const onPress = () => {
    navigation.navigate('AddExpense', {
      category: navigation.getParam('category'),
    });
  };

  return (
    <TouchableOpacity
      style={[expenseListStyles.container, styles.biggerContainer]}
      onPress={onPress}>
      <Text
        style={[
          expenseListStyles.text,
          expenseListStyles.bigText,
          styles.centerText,
        ]}>
        + Add expense
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  biggerContainer: {
    height: 80,
    alignItems: 'center',
  },
  centerText: {
    width: '100%',
    textAlign: 'center',
  },
});

export default AddExpenseListItem;
