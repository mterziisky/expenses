import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../config';
import Expense from '../../../domain/expense.interface';

type Props = {
  expense: Expense;
};

const ExpenseListItem = ({expense}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.text, styles.bigText]}>
        {expense.name} : {expense.price}
      </Text>
      <Text style={styles.text}>{expense.date}</Text>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.darkest,
    width: '100%',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: 12,
    paddingVertical: 3,
    borderBottomColor: colors.white,
    borderBottomWidth: 1,
  },
  text: {
    color: colors.white,
    fontWeight: 'bold',
  },
  bigText: {
    fontSize: 24,
  },
});

export default ExpenseListItem;
