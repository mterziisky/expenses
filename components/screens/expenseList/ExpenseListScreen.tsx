import React, {useContext} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import ExpenseListItem from './ExpenseListItem';
import AddExpenseListItem from './AddExpenseListItem';
import {colors} from '../../../config';
import {getExpensesForCategory} from '../../../utilities/data';
import DataContext from '../../../contexts/DataContext';

const ExpenseListScreen = ({navigation}) => {
  const {state} = useContext(DataContext);

  const renderExpenses = () => {
    const categoryId = navigation.getParam('category').id;
    const relevantExpenses = getExpensesForCategory(categoryId, state.expenses);

    return relevantExpenses
      .sort((a, b) => (a.date > b.date ? -1 : 1))
      .map(expense => <ExpenseListItem key={expense.id} expense={expense} />);
  };

  return (
    <ScrollView style={styles.containerInner} showsVerticalScrollIndicator>
      <AddExpenseListItem navigation={navigation} />
      {renderExpenses()}
    </ScrollView>
  );
};

ExpenseListScreen.navigationOptions = ({navigation}) => {
  const category = navigation.getParam('category');

  return {
    title: category.name,
  };
};

const styles = StyleSheet.create({
  containerInner: {
    backgroundColor: colors.darkest,
    width: '100%',
    height: '100%',
  },
});

export default ExpenseListScreen;
