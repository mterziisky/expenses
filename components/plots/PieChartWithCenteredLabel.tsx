import React, {useContext, useMemo} from 'react';
import {StyleSheet} from 'react-native';
import {PieChart} from 'react-native-svg-charts';
import {G, Line, Rect, Text} from 'react-native-svg';
import DataContext from '../../contexts/DataContext';
import {getExpensesForCategory} from '../../utilities/data';
import {colors} from '../../config';

const PieChartWithLabelExample = () => {
  const {state} = useContext(DataContext);
  const {categories, expenses} = state;

  const categoriesData = categories.map(c => ({
    value: getExpensesForCategory(c.id, expenses).reduce(
      (acc, curr) => acc + curr.price,
      0,
    ),
    name: c.name,
  }));
  const randomColor = () => {
    const r = Math.floor(Math.random() * 126) + 125;
    const g = Math.floor(Math.random() * 126) + 125;
    const b = Math.floor(Math.random() * 126) + 125;

    return `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`;
  };

  const randomColors = useMemo(() => {
    return Array.from({length: categories.length}, randomColor);
  }, [categories.length]);

  const pieData = categoriesData
    .filter(c => c.value > 0)
    .map((c, index) => ({
      value: c.value,
      svg: {fill: randomColors[index]},
      key: `pie-${index}`,
      name: c.name,
    }));

  const Labels = ({slices}) => {
    return slices.map(slice => {
      const {labelCentroid, pieCentroid, data} = slice;
      const rectWidth = 40;
      const rectHeight = 20;

      return (
        <G key={data.key}>
          <Line
            x1={labelCentroid[0]}
            y1={labelCentroid[1]}
            x2={pieCentroid[0]}
            y2={pieCentroid[1]}
            stroke={data.svg.fill}
          />
          <Rect
            x={labelCentroid[0] - rectWidth / 2}
            y={labelCentroid[1] - rectHeight / 2}
            width="40"
            height="20"
            fill={data.svg.fill}
          />
          <Text
            x={labelCentroid[0]}
            y={labelCentroid[1]}
            fill={colors.white}
            textAnchor="middle"
            alignmentBaseline={'middle'}
            fontSize={8}
            stroke={colors.darkest}
            strokeWidth={0.4}>
            {data.name}
          </Text>
        </G>
      );
    });
  };

  return (
    <PieChart
      style={styles.pieChart}
      data={pieData}
      innerRadius={20}
      outerRadius={55}
      labelRadius={80}
      animate
      animationDuration={800}>
      <Labels />
    </PieChart>
  );
};

const styles = StyleSheet.create({
  pieChart: {
    height: 200,
  },
});

export default PieChartWithLabelExample;
