import Expense from 'domain/expense.interface';

export const getExpensesForCategory = (
  categoryId: number,
  expensesToSearch: Expense[],
) => expensesToSearch.filter(e => e.categoryId === categoryId);
