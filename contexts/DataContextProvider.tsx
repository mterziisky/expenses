import React, {useReducer} from 'react';
import moment from 'moment';
import DataContext from './DataContext';
import Category from '../domain/category.interface';
import Expense from '../domain/expense.interface';

const CATEGORIES: Category[] = [
  {
    name: 'Food',
    id: 0,
  },
  {
    name: 'Technology',
    id: 1,
  },
];

const EXPENSES: Expense[] = [
  {
    name: 'Apple',
    price: 0.5,
    date: moment()
      .subtract(1, 'week')
      .format('YYYY-MM-DD'),
    id: 0,
    categoryId: 0,
  },
  {
    name: 'Car',
    price: 2000,
    date: moment()
      .subtract(2, 'days')
      .format('YYYY-MM-DD'),
    id: 1,
    categoryId: 1,
  },
  {
    name: 'Water',
    price: 1,
    date: moment().format('YYYY-MM-DD'),
    id: 2,
    categoryId: 0,
  },
  {
    name: 'Restaurant',
    price: 523,
    date: moment()
      .subtract(1, 'week')
      .format('YYYY-MM-DD'),
    id: 3,
    categoryId: 0,
  },
];

type State = {
  categories: Category[];
  expenses: Expense[];
};

type AddCategoryAction = {
  type: 'ADD_CATEGORY';
  category: Category;
};

type AddExpenseAction = {
  type: 'ADD_EXPENSE';
  expense: Expense;
};

type Action = AddCategoryAction | AddExpenseAction;

type Reducer = (state: State, action: Action) => State;

const reducer: Reducer = (prevState: State, action: Action) => {
  switch (action.type) {
    case 'ADD_CATEGORY':
      return {
        ...prevState,
        categories: [
          ...prevState.categories,
          {
            ...action.category,
            id: Math.max(...prevState.categories.map(c => c.id), -1) + 1,
          },
        ],
      };
    case 'ADD_EXPENSE':
      return {
        ...prevState,
        expenses: [
          ...prevState.expenses,
          {
            ...action.expense,
            id: Math.max(...prevState.expenses.map(e => e.id), -1) + 1,
          },
        ],
      };
    default:
      return prevState;
  }
};

const DataContextProvider = ({children}) => {
  const initialState: State = {
    categories: CATEGORIES,
    expenses: EXPENSES,
  };

  const [state, dispatch] = useReducer<Reducer>(reducer, initialState);

  const contextValue = {
    state,
    dispatch,
  };

  return (
    <DataContext.Provider value={contextValue}>{children}</DataContext.Provider>
  );
};

export default DataContextProvider;
