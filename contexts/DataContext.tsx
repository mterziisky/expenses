import React from 'react';
import Expense from '../domain/expense.interface';
import Category from '../domain/category.interface';

type State = {
  expenses: Expense[];
  categories: Category[];
};

type AddCategoryAction = {
  type: 'ADD_CATEGORY';
  category: Category;
};

type AddExpenseAction = {
  type: 'ADD_EXPENSE';
  expense: Expense;
};

type Action = AddCategoryAction | AddExpenseAction;
type Dispatch = (action: Action) => void;

type ContextType = {
  state: State;
  dispatch: Dispatch;
};

const DataContext = React.createContext<ContextType>({
  state: {},
  dispatch: () => {},
});

export default DataContext;
