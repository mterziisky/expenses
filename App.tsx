import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import DataContextProvider from './contexts/DataContextProvider';
import DashboardScreen from './components/screens/dashboard/DashboardScreen';
import AddCategoryScreen from './components/screens/AddCategoryScreen';
import {colors} from './config';
import ExpenseListScreen from './components/screens/expenseList/ExpenseListScreen';
import AddExpenseScreen from './components/screens/AddExpenseScreen';

const MainNavigator = createStackNavigator(
  {
    Dashboard: {screen: DashboardScreen},
    AddCategory: {screen: AddCategoryScreen},
    ExpenseList: {screen: ExpenseListScreen},
    AddExpense: {screen: AddExpenseScreen},
  },
  {
    initialRouteName: 'Dashboard',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: colors.lightest,
      },
      headerTintColor: colors.white,
      headerTitleStyle: {
        fontWeight: 'bold',
        color: colors.white,
      },
    },
  },
);

const AppNavigator = createAppContainer(MainNavigator);

const App = () => (
  <DataContextProvider>
    <AppNavigator />
  </DataContextProvider>
);

export default App;
